﻿
(function () {
    angular.module('UIDS_AADHAAR_App').config(config);
    function config($stateProvider, $urlRouterProvider, $logProvider, $httpProvider, $provide) {

        $urlRouterProvider.
        when("", '/');

        $stateProvider
          .state('home', {
              url: '/',
              templateUrl: '/App/Home/Views/index.html',
              controller: 'home',
              controllerAs: 'vm'
          })
      .state('secure', {
          url: '/secure',
          templateUrl: '/App/Seguridad/Views/secure.html',
          controller: 'SecureController',
          controllerAs: 'vm'
      })
        .state('error404', {
            url: '/',
            templateUrl: '/App/Home/Views/404.html'
        }).state('principal', {
            url: '/principal',
            templateUrl: '/App/Home/Views/principal.html',
            controller: 'principal',
            controllerAs: 'vm',
        }).state('registro', {
            url: '/registro',
            templateUrl: '/App/Registro/Views/index.html',
            controller: 'registroIndexController',
            controllerAs: 'vm',
        })
        .state('crear', {
            url: '/registro/nuevo',
            templateUrl: '/App/Registro/Views/modalCrearEditar.html',
            controller: 'modalCrearController',
            controllerAs: 'vm',
        })
        .state('mantenimientos', {
            url: '/mantenimientos',
            templateUrl: '/App/Mantenimientos/Views/index.html',
            controller: 'mantUsuariosController',
            controllerAs: 'vm'
        })

        //.state('Registro', {
        //    url: '/Registro',
        //    abstract: true,
        //    template: '<div ui-view />',
        //    data: {
        //        moduloId: 1
        //    }
        //})
        
    }
})();




