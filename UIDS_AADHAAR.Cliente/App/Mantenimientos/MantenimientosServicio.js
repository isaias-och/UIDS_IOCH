﻿(function () {

    angular.module("UIDS_AADHAAR_App").service("MantenimientosServicio", MantenimientosServicio);

    function MantenimientosServicio($http) {

        var api = "http://localhost:55711/api";
        

        var svc = this;

        svc.obtenerTodos = obtenerTodos;

        function obtenerTodos() {

            var url = api + "/usuarios";
            return $http.get(url).success(function (result) { return result.data });
            //return $http.get(url, { headers: getAuthorizationHeader() }).then(function (result) { return result.data });

        }

        

        function getAuthorizationHeader() {
            // obtiene el header de HTTP necesario para enviar el token 
            // de autenticación para la api.
            //
            // auth es una variable global, definida por /app/auth.js
            return { 'Authorization': 'Bearer ' + auth.currentUser().access_token };
        }
    }
    

})();