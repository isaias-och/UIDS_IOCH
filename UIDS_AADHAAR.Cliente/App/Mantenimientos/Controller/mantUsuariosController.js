﻿(function () {
    angular.module('UIDS_AADHAAR_App').controller('mantUsuariosController', mantUsuariosController);

    function mantUsuariosController($http, $state, $scope, $rootScope, $modal, MantenimientosServicio) {
        var vm = this;
        var vm = $scope.controller = this;
        vm.titulo = "REGISTRO DE USUARIOS";

        vm.usuarios = [];

        init();

        function init() {

            obtenerUsuario();

        }

        function obtenerUsuario() {
            MantenimientosServicio.obtenerTodos().then(cargarUsuarios);

        }

        function cargarUsuarios(usuarios) {

            console.log(usuarios);
        }

        vm.Login = function () {
            $state.transitionTo('principal');
            //$location.path('/principal');
        }

        vm.CrearPersona = function () {

            $state.transitionTo('crear');
        }

        function agregarInsumo() {

            console.log("insumo agregado");
        }

    }

})();