﻿(function () {

    angular.module('UIDS_AADHAAR_App').controller('modalCrearController', modalCrearController);

    function modalCrearController($log, $http, $state, $scope, $rootScope) {
        var vm = $scope.controller = this;
        vm.titulo = "NUEVA PERSONA";

        //vm.criterio = null;
        //vm.buscarInsumos = buscar;
        //vm.insumo = null;
        //vm.presentacion = null;
        ////Campos RSI
        //vm.renglonRsi = null;
        //vm.nombreRsi = null;
        //vm.presentacionRsi = null;
        //vm.estadoDetalle = null;
        //vm.estadoDetalle = data.estadoDetalle;
        //vm.idProyeccion = null;
        //vm.idProyeccion = data.proyeccionId;
        //vm.cancelar = cancelar;
        //vm.aceptar = aceptar;

        //function buscar() {
        //    vm.grid.dataSource.read();
        //}

        //function cancelar() {
        //    $modalInstance.dismiss();
        //}

        //function aceptar() {
        //    if (vm.sinRenglonActivo) {
        //        aceptarSinRenglon()
        //    }
        //    else if (vm.catalogoActivo) {
        //        insertarInsumo()
        //    }
        //}

        //function aceptarSinRenglon() {
        //    console.log("insertar Insumo RSI     ESTADO DETALLE", vm.estadoDetalle);
        //    var model = {
        //        renglon: vm.renglonRsi,
        //        NombreInsumo: vm.nombreRsi,
        //        NombrePresentacion: vm.presentacionRsi,
        //        idProyeccion: vm.idProyeccion,
        //        IdEstado: vm.estadoDetalle,
        //        precio: null
        //    };
        //    var detalleRsiUrl = config.server.apiUrl + 'api/instituciones/' + $rootScope.institucion.id + '/proyecciones/' + vm.idProyeccion + '/detalles/Rsi';
        //    $http.post(detalleRsiUrl, model).success(cerrarModal).error(mostrarError);

        //}

        //function insertarInsumo() {
        //    console.log("insertar Insumo SIGES ESTADO DETALLE", vm.estadoDetalle);
        //    var model = {
        //        Codigo: vm.insumo.codigo,
        //        Nombre: vm.insumo.nombre,
        //        Caracteristicas: vm.insumo.caracteristicas,
        //        Renglon: vm.insumo.renglon
        //    };
        //    var insumoUrl = config.server.apiUrl + 'api/instituciones/' + $rootScope.institucion.id + '/insumos'
        //    $http.post(insumoUrl, model).success(insertarPresentacion).error(mostrarError);

        //}

        //function insertarPresentacion() {
        //    var presentacion = vm.detailGrid.dataItem(vm.detailGrid.select());
        //    vm.presentacion = presentacion;
        //    var model = {
        //        Codigo: vm.presentacion.codigo,
        //        Nombre: vm.presentacion.nombre,
        //        CodigoInsumo: vm.insumo.codigo
        //    };
        //    var presentacionUrl = config.server.apiUrl + 'api/instituciones/' + $rootScope.institucion.id + '/presentaciones'
        //    $http.post(presentacionUrl, model).success(aceptarDelCatalogo).error(mostrarError);

        //}

        //function aceptarDelCatalogo() {

        //    var model = {
        //        renglon: vm.insumo.renglon,
        //        idProyeccion: vm.idProyeccion,
        //        codigoInsumo: vm.insumo.codigo,
        //        IdEstado: vm.estadoDetalle,
        //        codigoPresentacion: vm.presentacion.codigo,
        //        precio: null
        //    };
        //    var detalleUrl = config.server.apiUrl + 'api/instituciones/' + $rootScope.institucion.id + '/proyecciones/' + vm.idProyeccion + '/detalles/Siges';
        //    $http.post(detalleUrl, model).success(cerrarModal).error(mostrarError);
        //    //$http.post(/* insumo_siges, presentacion_siges, proyeccion_detalle */).success(cerrarModal).error(mostarError);
        //}




        //function cerrarModal(insumo) {
        //    $modalInstance.close(insumo);
        //}

        //function mostrarError() {
        //    errorServicio.mostrarEnToastr.apply(this, arguments);
        //}

        //vm.gridOptions = {
        //    dataSource: {
        //        type: "get",
        //        dataType: "json",
        //        transport: {
        //            read: function (options) {
        //                if (!vm.criterio) {
        //                    options.success([]);
        //                    return;
        //                }
        //                insumosServicio.buscar(vm.criterio).success(options.success);
        //            }
        //        },

        //    },
        //    pageable: {
        //        buttonCount: 3,
        //        pageSize: 20,
        //        pageSizes: [20, 50, 100],
        //        refresh: true,
        //        serverPaging: false
        //    },
        //    scrollable: true,
        //    sortable: true,
        //    height: 480,
        //    Resizable: true, columns: [
        //           { field: "codigo", title: "Código" },
        //           { field: "nombre", title: "Nombre" },
        //           { field: "caracteristicas", title: "Características" },
        //           { field: "renglon", title: "Renglón" }
        //    ],
        //    detailTemplate: renderizarDetailTemplate

        //};

        //vm.detailGridOptions = {
        //    dataSource: {
        //        type: "get",
        //        dataType: "json",
        //        transport: {
        //            read: function (options) {
        //                if (!vm.insumo) {
        //                    options.success([]);
        //                    return;
        //                }
        //                presentacionesServicio.obtenerPorInsumo(vm.insumo.codigo).success(options.success);
        //            }
        //        }
        //    },
        //    selectable: true,
        //    Resizable: true, columns: [
        //           { field: "codigo", title: "Código" },
        //           { field: "nombre", title: "Nombre" },
        //    ],
        //}

        //function renderizarDetailTemplate(insumo) {
        //    vm.insumo = insumo;
        //    return '<div kendo-grid="vm.detailGrid" k-options="vm.detailGridOptions"></div>'
        //}

        //vm.editar = function () {
        //    var presentacion = vm.detailGrid.dataItem(vm.detailGrid.select());
        //    console.log('Insumo seleccionado: ' + vm.insumo);
        //    console.log('Presentacion seleccionada: ' + presentacion);

        //    // aqui se puede hacer un post a un nuevo action que se cree en el ProyeccionesController, por ejemplo
        //    // post: api/instituciones/7/proyecciones/14/necesidades

        //}


    }
})();

