﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Web.Http;
using UIDS_AADHAAR.Core.Modulos.Mantenimientos.Servicios;

namespace UIDS_AADHAAR.Cliente.Controllers.Seguridad
{
    [RoutePrefix("api/Mantenimiento/usuarios")]
    public class SeguridadController : ApiController
    {
        private readonly UsuarioServicio servicio;

        public SeguridadController()
        {
            servicio = new UsuarioServicio();

        }

    [Route("todos")]
    public IHttpActionResult getUsuariosTodos()
        {
            var usuarios = servicio.obtenerTodos();
            return Ok(usuarios);


        }

    }
}