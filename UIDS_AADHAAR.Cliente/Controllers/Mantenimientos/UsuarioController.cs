﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UIDS_AADHAAR.Cliente.Modelos.Mantenimientos;
using UIDS_AADHAAR.Cliente.App_Start;

namespace UIDS_AADHAAR.Cliente.Controllers.Mantenimientos
{
    public class UsuarioController : Controller
    {

        private UIDS_AADHAAR_Context _context = null;

        public UsuarioController()
        {
            _context = new UIDS_AADHAAR_Context();
        }

        
        // GET: Usuario
        public ActionResult Index()
        {
            List < UsuarioModel > Usuarios = _context.Usuarios.ToList();
            return Json (new { list = Usuarios}, JsonRequestBehavior.AllowGet);
            
        }
    }
}