﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace UIDS_AADHAAR.Cliente.App_Start
{
    public class UIDS_AADHAAR_Context
    {

        public DbSet<UIDS_AADHAAR.Cliente.Modelos.Mantenimientos.UsuarioModel> Usuarios { get; set; }
            
    }
}