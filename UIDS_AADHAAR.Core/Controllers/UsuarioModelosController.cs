﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UIDS_AADHAAR.Core.Models;
using UIDS_AADHAAR.Core.Modulos.Mantenimientos.Modelos;

namespace UIDS_AADHAAR.Core.Controllers
{
    public class UsuarioModelosController : Controller
    {
        private UIDS_AADHAARCoreContext db = new UIDS_AADHAARCoreContext();

        // GET: UsuarioModelos
        public ActionResult Index()
        {
            return View(db.UsuarioModeloes.ToList());
        }

        // GET: UsuarioModelos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioModelo usuarioModelo = db.UsuarioModeloes.Find(id);
            if (usuarioModelo == null)
            {
                return HttpNotFound();
            }
            return View(usuarioModelo);
        }

        // GET: UsuarioModelos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UsuarioModelos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Password")] UsuarioModelo usuarioModelo)
        {
            if (ModelState.IsValid)
            {
                db.UsuarioModeloes.Add(usuarioModelo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(usuarioModelo);
        }

        // GET: UsuarioModelos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioModelo usuarioModelo = db.UsuarioModeloes.Find(id);
            if (usuarioModelo == null)
            {
                return HttpNotFound();
            }
            return View(usuarioModelo);
        }

        // POST: UsuarioModelos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Password")] UsuarioModelo usuarioModelo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuarioModelo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(usuarioModelo);
        }

        // GET: UsuarioModelos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioModelo usuarioModelo = db.UsuarioModeloes.Find(id);
            if (usuarioModelo == null)
            {
                return HttpNotFound();
            }
            return View(usuarioModelo);
        }

        // POST: UsuarioModelos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UsuarioModelo usuarioModelo = db.UsuarioModeloes.Find(id);
            db.UsuarioModeloes.Remove(usuarioModelo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
