﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace UIDS_AADHAAR.Core.Modulos
{
    public class ConfiguracionServicio
    {
            private const string ConnectionStringName = "CadenaDeConexion";

            public string NombreConexionPorDefecto { get { return ConnectionStringName; } }

            public virtual string UIDSConnectionString
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
                }
            }

            public virtual string UIDSConnectionProvider
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings[ConnectionStringName].ProviderName;
                }
            }
    
            public virtual string PathArchivos
            {
                get { return ConfigurationManager.AppSettings["UIDS.Archivos"]; }
            }

            public virtual string PathArchivosTemporales
            {
                get { return ConfigurationManager.AppSettings["UIDS.Archivos.PathTemporal"]; }
            }

            public virtual string ObtenerConnectionString(string nombre)
            {
                var cs = ConfigurationManager.ConnectionStrings[nombre];
                if (cs == null)
                {
                    throw new InvalidOperationException("No existe la cadena de conexion con nombre " + nombre);
                }

                return ConfigurationManager.ConnectionStrings[nombre].ConnectionString;
            }

            public virtual string ObtenerConnectionProvider(string nombre)
            {
                var cs = ConfigurationManager.ConnectionStrings[nombre];
                if (cs == null)
                {
                    throw new InvalidOperationException("No existe la cadena de conexion con nombre " + nombre);
                }

                return ConfigurationManager.ConnectionStrings[nombre].ProviderName;
            }
        }
    }
