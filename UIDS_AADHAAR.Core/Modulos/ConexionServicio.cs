﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data;

namespace UIDS_AADHAAR.Core.Modulos
{
    public class ConexionServicio
    {

            private readonly string nombreConexion;
            private readonly string connectionString;
            private readonly string nombreProveedor;
            private readonly DbProviderFactory dbFactory;

            public ConexionServicio(string nombreConexion = null, ConfiguracionServicio configuracion = null)
            {
                configuracion = configuracion ?? new ConfiguracionServicio();

                this.nombreConexion = nombreConexion ?? configuracion.NombreConexionPorDefecto;
                this.connectionString = configuracion.ObtenerConnectionString(this.nombreConexion);
                this.nombreProveedor = configuracion.ObtenerConnectionProvider(this.nombreConexion);
                this.dbFactory = DbProviderFactories.GetFactory(this.nombreProveedor);
            }

            public virtual IDbConnection AbrirConexion()
            {
                var conexion = dbFactory.CreateConnection();
                conexion.ConnectionString = connectionString;
                conexion.Open();
                return conexion;
            }

        }
    }































