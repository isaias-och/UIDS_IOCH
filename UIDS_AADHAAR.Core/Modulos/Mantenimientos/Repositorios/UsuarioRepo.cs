﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UIDS_AADHAAR.Core.Modulos.Mantenimientos.Modelos;

namespace UIDS_AADHAAR.Core.Modulos.Mantenimientos.Repositorios
{
    public class UsuarioRepo
    {
        private readonly ConexionServicio conexionServicio;

        internal UsuarioRepo()
        {
            conexionServicio = new ConexionServicio();

        }


        public IEnumerable<UsuarioModelo> obtenerUsuarios()
        {
            const string sql = @"
                SELECT * FROM MNT_USUARIO
            ";

            using (var con = conexionServicio.AbrirConexion())
            {
                return con.Query<UsuarioModelo>(sql);

            }

        }



    }
}