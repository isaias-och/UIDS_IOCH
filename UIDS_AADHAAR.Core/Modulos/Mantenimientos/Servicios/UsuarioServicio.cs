﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UIDS_AADHAAR.Core.Modulos.Mantenimientos.Modelos;
using UIDS_AADHAAR.Core.Modulos.Mantenimientos.Repositorios;

namespace UIDS_AADHAAR.Core.Modulos.Mantenimientos.Servicios
{
    public class UsuarioServicio
    {

        private readonly UsuarioRepo usuarioRepo;
        private readonly UsuarioModelo usuarioModelo;


        public UsuarioServicio()
        {
            usuarioRepo = new UsuarioRepo();
            usuarioModelo = new UsuarioModelo();

        }

        public IEnumerable<UsuarioModelo> obtenerTodos()
        {

            return usuarioRepo.obtenerUsuarios();

        }
        
    }
}