﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UIDS_AADHAAR.Server.Entidades
{
    [Table("MNT_USUARIO")]
    public class Usuario
    {
        public int ID_USUARIO { get; set; }

        public int ID_ENTIDAD { get; set; }

        public int ID_TIPO_PERSONA { get; set; }

        [StringLength(250)]
        public string NOMBRE { get; set; }

        [StringLength(50)]
        public string CORREO { get; set; }

        [StringLength(50)]
        public string PASSWORD { get; set; }
    }
}