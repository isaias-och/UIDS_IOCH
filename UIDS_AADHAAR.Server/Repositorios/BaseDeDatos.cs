﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UIDS_AADHAAR.Server.Entidades;

namespace UIDS_AADHAAR.Server.Repositorios
{
    public class BaseDeDatos : DbContext
    {

        public DbSet<Usuario> Usuarios {get; set;}

        static BaseDeDatos()
        {
            Database.SetInitializer(new NullDatabaseInitializer<BaseDeDatos>());

        }

        public BaseDeDatos() : base("CadenaDeConexion")
        {
        
        }


    }
}